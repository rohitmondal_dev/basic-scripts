#!/bin/bash

export CONTAINER_NAME="MongoDB"
export BACKUP_LOCATION="/home/jnctc/db_backups"
export DOCKER_LOCATION="/data/backups"
export DATABASE_NAME="jnctc"
export FILE_NAME="backup-20220403195456"

echo Starting Database Restore Process ...

docker cp ${BACKUP_LOCATION}/${FILE_NAME} ${CONTAINER_NAME}:${DOCKER_LOCATION}/${FILE_NAME}
docker exec -i ${CONTAINER_NAME} mongorestore --db ${DATABASE_NAME} ${DOCKER_LOCATION}/${FILE_NAME}/${DATABASE_NAME}
docker exec MongoDB rm -rf ${DOCKER_LOCATION}/${FILE_NAME}
echo Done...
