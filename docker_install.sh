clear
echo "step 1 :. Preparing........"
sudo snap disable docker
sudo remove docker
sudo snap remove docker
sudo apt-get remove docker docker-engine docker.io containerd runc
echo "step 2 :. Update Instance....."
sudo apt-get update
echo "Step 3 :. Installing Docker....."
sudo apt-get install docker-ce docker-ce-cli containerd.io
echo "step 4 :. Installing Docker Compose----------"
sudo apt install docker-compose
echo "step 5 :. Add Group"
sudo addgroup --system docker
echo "step 6 :. Add User :  $USER"
sudo adduser $USER docker
newgrp docker
echo "Step 7:. Restarting Docker....."
sudo service docker start
echo "Docker Was Sucessfully Installed on this System"
